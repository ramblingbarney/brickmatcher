
import Foundation
import CoreLocation

struct Location: Codable {
    var lat: Double
    var lng: Double
    
    var coord: CLLocationCoordinate2D {
        return loc.coordinate
    }
    
    var loc: CLLocation {
        return CLLocation(latitude: lat, longitude: lng)
    }
}

struct Geometry: Codable {
    var location: Location
}

struct SearchResult: Codable {
    var icon: String
    var name: String
    var rating: Double
    var vicinity: String
    var geometry: Geometry
}

struct APIResponse: Codable {
    var results: [SearchResult]
    var status: String
}
