//
//  AppDelegate.swift
//  BrickMatcherV4
//
//  Created by The App Experts on 25/11/2019.
//  Copyright © 2019 Conor O'Dwyer. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let defaults = UserDefaults.standard
        let seenHelpInformation = defaults.bool(forKey: "seenHelpInformation")
        
        let homeViewController = HomeViewController()
        let homeIcon = UITabBarItem(title: "", image: UIImage(systemName: "house"), tag: 0)
        homeViewController.tabBarItem = homeIcon
        
        let mapViewController = MapViewController()
        let mapIcon = UITabBarItem(title: "", image: UIImage(systemName: "map"), tag: 1)
        mapViewController.tabBarItem = mapIcon
        
        let imageViewController = ImageViewController()
        let imageIcon = UITabBarItem(title: "", image: UIImage(systemName: "camera.on.rectangle"), tag: 2)
        imageViewController.tabBarItem = imageIcon
        
        let informationViewController = InformationViewController()
        let informationIcon = UITabBarItem(title: "", image: UIImage(systemName: "info.circle"), tag: 2)
        informationViewController.tabBarItem = informationIcon
    
        let controllers = [homeViewController, mapViewController, imageViewController, informationViewController]

        let tabBarController = UITabBarController()
        
        tabBarController.viewControllers = controllers
        
        tabBarController.viewControllers = controllers.map { UINavigationController(rootViewController: $0)}
        
        
        // Set the focus if the User has previously opened the app with focus on Information View Controller
        if seenHelpInformation {
            tabBarController.selectedIndex = 0
        } else {
            tabBarController.selectedIndex = 3
        }
        
        window?.rootViewController = tabBarController
        window?.makeKeyAndVisible()
        
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    //    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    //        // Called when a new scene session is being created.
    //        // Use this method to select a configuration to create the new scene with.
    //        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    //    }
    //
    //    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    //        // Called when the user discards a scene session.
    //        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    //        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    //    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "BrickMatcherV4")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
