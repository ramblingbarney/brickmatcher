//
//  ImageViewController.swift
//  BrickMatcherV4
//
//  Created by The App Experts on 25/11/2019.
//  Copyright © 2019 Conor O'Dwyer. All rights reserved.
//
import AVFoundation
import UIKit

public protocol ImagePickerDelegate: class {
    func didSelect(image: UIImage?)
}

class ImageViewController: UIViewController {
    
    var imagePlaceholder: UIImage = UIImage(named: "Wall-Brick-Boral-Bricks-NSW-Elan-Standard2")!
    var imageView: UIImageView!
    var imagePicker: ImagePicker!
    var selectImage: UIButton!
    var matchImage: UIButton!
    var enableMatch: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .darkGray
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Search", style: .plain, target: self, action: #selector(showImagePicker))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Match", style: .plain, target: self, action: #selector(showMatchType))
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        setupUI()
        constraints()
        enableMatch = false
        self.imageView.contentMode = .scaleAspectFill
        self.imageView.clipsToBounds = true
    }
    
    
    private func setupUI() {
        
        imageView = UIImageView(image: imagePlaceholder)
        imageView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        view.addSubview(imageView)
    }
    
    private func constraints() {
        
        let margins = view.layoutMarginsGuide
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 0.0).isActive = true
        imageView.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -0.0).isActive = true
        imageView.topAnchor.constraint(equalTo: margins.topAnchor, constant: 3.0).isActive = true
        imageView.bottomAnchor.constraint(equalTo: margins.bottomAnchor, constant: -3.0).isActive = true
        view.addSubview(imageView)
    }
    
    @objc func showImagePicker(_ sender: UIButton) {

        self.imagePicker.present(from: sender)
    }
    
    @objc func showMatchType(_ sender: UIButton) {
        
        if enableMatch {
            
            print("show match alert controller")
            
//            imageView
//            
//            
//            let newViewController = MapListViewController()
//            
//            newViewController.delegate = self
//            self.navigationController?.pushViewController(newViewController, animated: true)
//            
//            
            
            
            
        } else {
            
            noImageAlert()
        }
    }
    
    private func noImageAlert() {
        
        let alertController = UIAlertController(title: "Please Select Image", message: "", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
            print("Pressed OK")
        }
        alertController.addAction(OKAction)
        present(alertController, animated: true, completion: nil)
    }
}

extension ImageViewController: ImagePickerDelegate {
    
    func didSelect(image: UIImage?) {
        self.imageView.image = image
        enableMatch = true
    }
}

open class ImagePicker: NSObject {
    
    private let pickerController: UIImagePickerController
    private weak var presentationController: UIViewController?
    private weak var delegate: ImagePickerDelegate?
    
    public init(presentationController: UIViewController, delegate: ImagePickerDelegate) {
        self.pickerController = UIImagePickerController()
        
        super.init()
        
        self.presentationController = presentationController
        self.delegate = delegate
        
        self.pickerController.delegate = self
        self.pickerController.allowsEditing = true
        self.pickerController.mediaTypes = ["public.image"]
    }
    
    private func action(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }
        
        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            self.pickerController.sourceType = type
            self.presentationController?.present(self.pickerController, animated: true)
        }
    }
    
    public func present(from sourceView: UIView) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if let action = self.action(for: .camera, title: "Take photo") {
            alertController.addAction(action)
        }
        if let action = self.action(for: .savedPhotosAlbum, title: "Camera roll") {
            alertController.addAction(action)
        }
        if let action = self.action(for: .photoLibrary, title: "Photo library") {
            alertController.addAction(action)
        }
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alertController.popoverPresentationController?.sourceView = sourceView
            alertController.popoverPresentationController?.sourceRect = sourceView.bounds
            alertController.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }
        
        self.presentationController?.present(alertController, animated: true)
    }
    
    private func pickerController(_ controller: UIImagePickerController, didSelect image: UIImage?) {
        controller.dismiss(animated: true, completion: nil)
        
        self.delegate?.didSelect(image: image)
    }
}

extension ImagePicker: UIImagePickerControllerDelegate {
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.pickerController(picker, didSelect: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        guard let image = info[.editedImage] as? UIImage else {
            return self.pickerController(picker, didSelect: nil)
        }
        self.pickerController(picker, didSelect: image)
    }
}

extension ImagePicker: UINavigationControllerDelegate {
    
}
