//
//  InformationViewController.swift
//  BrickMatcherV4
//
//  Created by The App Experts on 27/11/2019.
//  Copyright © 2019 Conor O'Dwyer. All rights reserved.
//

import UIKit

class InformationViewController: UITableViewController {
    
    let cellId = "sdlfjowieurewfn34898442249sd;ds;lfkds;f47824dslaksjfs;ad"
    
    let entries = [(title: "First Help Step", image: "green_circle"),
                   (title: "Second Help Step", image: "blue_square"),
                   (title: "Third Help Step", image: "black_diamond"),
                   (title: "Fourth Help Step", image: "double_black_diamond")]
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationItem.title = "How To Use Brick Matcher"
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        //        tableView.register(SubtitleTableViewCell.self, forCellReuseIdentifier: cellId)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        defaults.set(true, forKey: "seenHelpInformation")
    }
}

extension InformationViewController {
    
    // MARK: - UITableViewController methods
    
    override func numberOfSections(in tableView: UITableView) -> Int { return 1 }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // If the search bar is active, use the searchResults data.
        return entries.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // If the search bar is active, use the searchResults data.
        let entry = entries[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.textLabel?.text = entry.title
        cell.imageView?.image = UIImage(named: entry.image)
        return cell
    }
    
    class SubtitleTableViewCell: UITableViewCell {
        
        override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}
