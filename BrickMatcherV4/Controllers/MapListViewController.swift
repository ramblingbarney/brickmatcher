//
//  MapListViewController.swift
//  BrickMatcherV4
//
//  Created by The App Experts on 27/11/2019.
//  Copyright © 2019 Conor O'Dwyer. All rights reserved.
//

import UIKit
import MapKit

class MapListViewController: UITableViewController, MKMapViewDelegate {
    
    let searchController = UISearchController(searchResultsController: nil)
    var textFieldInsideSearchBar: UITextField!
    let cellId = "akshdkdsjfkljdslkfjdslkfjf498789375984375$$$"
    
    // A delegate to communicate to the MapViewController
    weak var delegate: CustomMapViewDelegate?
    
    // An image cache
    var imageCache: NSCache<NSString, UIImage>!
    
    // An array of SearchResult items
    private var tableViewData: Array<SearchResult> = []
    
    // A reference to the GeoCoder
    let geocoder = CLGeocoder()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageCache = NSCache<NSString, UIImage>()
        navigationItem.title = "Search Location"
        setupSearchController()
        tableView.dataSource = self
        tableView.delegate = self
        textFieldInsideSearchBar.delegate = self
        textFieldInsideSearchBar.returnKeyType = .search
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    private func setupSearchController() {
        definesPresentationContext = true
        textFieldInsideSearchBar = searchController.searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar.backgroundColor = UIColor.white
        searchController.hidesNavigationBarDuringPresentation = false
        tableView.tableHeaderView = searchController.searchBar
    }
    
    // MARK: - Table View
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        
        // Configure the cell with the SearchResult and the imageCache
        cell.config(using: tableViewData[indexPath.row], imageCache: imageCache)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // When we select a row, we need to center on that item
        let selectedLocation = tableViewData[indexPath.row]
        delegate?.centerMap(on: selectedLocation.geometry.location.loc)
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension UITableViewCell {
    
    func config(using result: SearchResult, imageCache: NSCache<NSString, UIImage>) {
        
        // Set the name and vicinity
        textLabel?.text = result.name
        detailTextLabel?.text = result.vicinity
        
        // Configure the imageView
        imageView?.image = nil
        imageView?.contentMode = .scaleAspectFit
        
        // Get the URL of the icon
        guard let imageURL = URL(string: result.icon) else { return }
        
        // Use the file name as the cache key
        let key = imageURL.lastPathComponent as NSString
        
        // Attempt to get image from the cache
        if let image = imageCache.object(forKey: key) {
            imageView?.image = image
        } else {
            
            // Set the request to get the image
            let task = URLSession(configuration: .ephemeral).dataTask(with: imageURL) { (data, response, error) in
                
                // error handling
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                
                // Get the image from the data
                if let data = data, let image = UIImage(data: data) {
                    imageCache.setObject(image, forKey: key)
                    
                    // Update the image
                    DispatchQueue.main.async {
                        self.imageView?.image = image
                        self.setNeedsLayout()
                    }
                }
            }
            
            task.resume()
        }
        
    }
    
}

extension MapListViewController: UITextFieldDelegate {
    
    internal func textFieldShouldReturn(_ textFieldInsideSearchBar: UITextField) -> Bool {
        
        guard let text = textFieldInsideSearchBar.text else { return true }
        
        // GeoCode the address from the string
        geocoder.geocodeAddressString(text) { (placemarks, error) in
            
            // Get the placemark
            guard let foundPlacemark = placemarks?.last else { return print(error!.localizedDescription) }
            
            // Tell the delegate to update the map with the new placemark
            self.delegate?.updateMap(with: foundPlacemark)
            
            // Create the URL components
            guard var urlComps = URLComponents(string: "https://maps.googleapis.com/maps/api/place/search/json") else { return print("Cannot create URL Comps") }
            
            // Add the queries
            urlComps.queryItems = [
                URLQueryItem(name: "key", value: "AIzaSyAw0m3prCKzqP-zrWauU7DsXJgMDnbQY-Y"),
                URLQueryItem(name: "sensor", value: "false"),
                URLQueryItem(name: "types", value: "point_of_interest"),
                URLQueryItem(name: "radius", value: "10000"),
                URLQueryItem(name: "location", value: "\(foundPlacemark.location!.coordinate.latitude),\(foundPlacemark.location!.coordinate.longitude)")
            ]
            
            // Get the URL
            guard let googlSearchURL = urlComps.url else { return }
            
            // Get the data from the API
            let task = URLSession.shared.dataTask(with: googlSearchURL, completionHandler: { (data, response, error) in
                
                // Get the data
                guard let data = data else { return print(error?.localizedDescription ?? "") }
                
                do {
                    // Parse the data
                    let api_response = try JSONDecoder().decode(APIResponse.self, from: data)
                    self.tableViewData = api_response.results
                    
                    // Refresh the UI
                    DispatchQueue.main.async {
                        self.delegate?.updateAnnotations(from: api_response.results)
                        self.tableView.reloadData()
                    }
                    
                } catch {
                    print(error.localizedDescription)
                }
                
            })
            
            task.resume()
        }
        return true
    }
    
}


