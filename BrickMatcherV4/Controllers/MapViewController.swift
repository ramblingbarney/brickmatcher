//
//  MapViewController.swift
//  BrickMatcherV4
//
//  Created by The App Experts on 25/11/2019.
//  Copyright © 2019 Conor O'Dwyer. All rights reserved.
//

import UIKit
import MapKit

protocol CustomMapViewDelegate: class {
    
    func updateMap(with placemark: CLPlacemark)
    func centerMap(on location: CLLocation)
    func updateAnnotations(from locations: Array<SearchResult>)
}

class CustomAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String = "Title", subtitle: String = "Subtitle") {
        
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
}

class MapViewController: UIViewController {
    
    var mapView: MKMapView!
    
    var reuseID: String {
        return "CustomAnnotation"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createMapView()
        navigationItem.title = "Find Your Location"
        let searchItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.search, target: self, action: #selector(showMapListController))
        navigationItem.leftBarButtonItem = searchItem
        mapView.register(MKAnnotationView.self, forAnnotationViewWithReuseIdentifier: reuseID)
    }
    
    func createMapView() {
        mapView = MKMapView()
        let leftMargin: CGFloat = 0
        let topMargin:CGFloat = 0
        let mapWidth:CGFloat = view.bounds.width
        let mapHeight:CGFloat = view.bounds.height
        mapView.frame = CGRect(x: leftMargin, y: topMargin, width: mapWidth, height: mapHeight)
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        // Or, if needed, we can position map in the center of the view
        mapView.center = view.center
        
        view.addSubview(mapView)
    }
    
    @objc private func showMapListController(){
        
        let newViewController = MapListViewController()
        
        newViewController.delegate = self
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        let regionRadius = CLLocationDistance(1500)
        
        let region = MKCoordinateRegion(center: view.annotation!.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        
        mapView.setRegion(region, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let annotView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID, for: annotation)
        
        annotView.image = #imageLiteral(resourceName: "push_pin")
        
        return annotView
    }
    
}

extension MapViewController: CustomMapViewDelegate {
    
    func updateMap(with placemark: CLPlacemark) {
        
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let mapRegion = MKCoordinateRegion(center: placemark.location!.coordinate, span: span)
        
        mapView.setRegion(mapRegion, animated: true)
        
    }
    
    func centerMap(on location: CLLocation) {
        
        for annotation in mapView.selectedAnnotations {
            mapView.deselectAnnotation(annotation, animated: true)
        }
        
        let regionRadius = CLLocationDistance(1500)
        
        let region = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        
        mapView.setRegion(region, animated: true)
        
        for annotation in mapView.annotations {
            
            if location.distance(from: CLLocation(latitude: annotation.coordinate.latitude, longitude: annotation.coordinate.longitude)) < 0.01 {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.65) {
                    self.mapView.selectAnnotation(annotation, animated: true)
                }
            }
        }
    }
    
    func updateAnnotations(from locations: Array<SearchResult>) {
        
        mapView.removeAnnotations(mapView.annotations)
        
        mapView.addAnnotations(locations.map {
            CustomAnnotation.init(coordinate: $0.geometry.location.coord, title: $0.name, subtitle: $0.vicinity)
        })
        
    }
}
